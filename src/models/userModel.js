var mongoose = require('mongoose');

// Setup schema
var userSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    avatar: String,
    password: String,
    gender: Number,
    phone: String,
    create_date: {
        type: Date,
        default: Date.now
    }
});
module.exports.get = function (callback, limit) {
    User.find(callback).limit(limit);
}
// Export User model
var User = module.exports = mongoose.model('user', userSchema);
