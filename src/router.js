// Initialize express router
let router = require('express').Router();

// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome to my app crafted with love!',
    });
});

// ---------- User --------------//
// Import User controller
var userController = require('./controllers/userController.js');
// Contact routes
router.route('/user')
    // Params :
    .get(userController.index)
    // Params :
    //  {
    //      name:String,
    //      gender:Number,
    //      avatar:String,
    //      email:String,
    //      phone:String
    //  }
    .post(userController.new);

router.route('/users/:user_id')
    .get(userController.view)
    .patch(userController.update)
    .put(userController.update)
    .delete(userController.delete);

router.route('/user/login')
    .put(userController.login);











// ---------- File upload -----------//
var fileController = require('./controllers/fileController.js');
// File upload routes
router.route('/upload')
    // Params :
    //  {
    //      file:String
    //  }
    .post(fileController.new);


// Export API routes
module.exports = router;