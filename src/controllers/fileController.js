const { uploaddir } = require('../../config');
const path = require('path');
var multer = require('multer');

// Upload file

var storage =   multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './'+uploaddir+'/');
    },
    filename: function (req, file, callback) {
        console.log(file.originalname);
        callback(null, file.fieldname + '-' + Date.now() + '' +path.extname(file.originalname));
    }
});

var upload = multer({ storage : storage, limits: {fileSize: 1000000}}).single('file');

exports.new = function(req,res){
    upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        res.end(req.file.filename);
    });
}