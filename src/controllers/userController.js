// Import user model
User = require('../models/userModel');
const { uploaddir } = require('../../config');
const path = require('path');
var multer = require('multer');

// Upload file

var storage =   multer.diskStorage({
    destination: function (req, file, callback) {
        callback(null, './'+uploaddir+'/');
    },
    filename: function (req, file, callback) {
        console.log(file.originalname);
        callback(null, file.fieldname + '-' + Date.now() + '' +path.extname(file.originalname));
    }
});

var upload = multer({ storage : storage, limits: {fileSize: 1000000}}).single('file');


// Handle index actions
exports.index = function (req, res) {
    User.find(function (err, users) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "users retrieved successfully",
            data: users
        });
    });
};

// Handle create user actions
exports.new = async function (req, res) {
    var user = new User();
    upload(req,res,function(err) {
        if(err) {
            return res.end("Error uploading file.");
        }
        user.avatar = '/'+uploaddir+'/'+req.file.filename;
        user.name = req.body.name;
        user.gender = req.body.gender;
        user.email = req.body.email;
        user.phone = req.body.phone;
        user.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'New user created!',
                data: user
            });
        });
    });
};

// Handle view user info
exports.view = function (req, res) {
    User.findById(req.params.user_id, function (err, user) {
        if (err)
            res.send(err);
        res.json({
            message: 'user details loading..',
            data: user
        });
    });
};

// Handle update user info
exports.update = function (req, res) {
    User.findById(req.params.user_id, function (err, user) {
        if (err)
            res.send(err);
        user.name = req.body.name ? req.body.name : user.name;
        user.gender = req.body.gender;
        user.email = req.body.email;
        user.phone = req.body.phone;
        // save the user and check for errors
        user.save(function (err) {
            if (err)
                res.json(err);
            res.json({
                message: 'user Info updated',
                data: user
            });
        });
    });
};

// Handle delete user
exports.delete = function (req, res) {
    User.remove({
        _id: req.params.user_id
    }, function (err, user) {
        if (err)
            res.send(err);
        res.json({
            status: "success",
            message: 'user deleted'
        });
    });
};

// Handle login user
exports.put = function (req, res) {
    // User.remove({
    //     _id: req.params.user_id
    // }, function (err, user) {
    //     if (err)
    //         res.send(err);
    //     res.json({
    //         status: "success",
    //         message: 'user deleted'
    //     });
    // });
};