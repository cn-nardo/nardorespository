const { port, mport, dbname} = require('./config');


// -------- Import Config And Env -----------//
// Import express
let express = require('express');

// Import Body parser
let bodyParser = require('body-parser');

// Import Mongoose
let mongoose = require('mongoose');

// Connect to Mongoose and set connection variable
mongoose.connect('mongodb://127.0.0.1:'+mport+'/'+dbname, {useNewUrlParser: true});
var db = mongoose.connection;

// Initialize the app
let app = express();

// Launch app to listen to specified port
app.listen(port, function () {
    console.log("Running RestHub on port " + port);
});

// Configure bodyparser to handle post requests
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

// Import routes
let apiRoutes = require("./src/router.js");
// Use Api routes in the App
app.use('/api', apiRoutes);
